#!/usr/bin/python3

import argparse
import configparser
import csv
from os.path import expanduser

def convert_viking_csv_to_qif(input_file, output_file, account, category):
    with input_file as usage_report:
        if account:
            output_file.write('!Account\n')
            output_file.write('N'+account+'\n')
            output_file.write('^\n')
        output_file.write('!Type:Oth A\n')
        viking_usage = csv.DictReader(usage_report, delimiter=';')
        for row in viking_usage:
            if float(row['Price']) > 0:
                output_file.write('D'+row['Start_time'].split(' ',1)[0]+'\n')
                output_file.write('T-'+row['Price']+'\n')
                output_file.write('M'+row['Type']+' ')
                if row['Type'].startswith('Voice call'):
                    output_file.write(row['Duration human readable']+' '+row['To/From'])
                elif row['Type'].startswith('Text message'):
                    output_file.write(' '+row['To/From'])
                elif row['Type'].startswith('Data'):
                    output_file.write(row['Duration human readable'])
                else:
                    print('Warning: type '+row['Type']+' not implemented')
                output_file.write('\n')
                if category:
                    output_file.write('L'+category+'\n')
                output_file.write('^\n')
            elif float(row['Price']) < 0:
                print('Warning: refunds not implemented')

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Convert Mobile viking CSV usage report to QIF format')
    parser.add_argument('usage_report', type=argparse.FileType('r'))
    parser.add_argument('output', type=argparse.FileType('w'))
    parser.add_argument('-a', '--account', default=None, help='QIF account name (Asset account in GnuCash)')
    parser.add_argument('-c', '--category', default=None, help='Category for the transactions (Expense account in GnuCash)')
    parser.add_argument('--config', default=None, help='Optional configuration file')
    args = parser.parse_args()
    account = args.account
    category = args.category
    if not account or not category:
        config = configparser.ConfigParser()
        if args.config:
            config.read(args.config)
        else:
            config.read(expanduser('~/.vikingcsv2qif.conf'))
    if not account and 'vikingcsv2qif' in config and 'account' in config['vikingcsv2qif']:
        account = config['vikingcsv2qif']['account']
    if not category and 'vikingcsv2qif' in config and 'category' in config['vikingcsv2qif']:
        category = config['vikingcsv2qif']['category']
    convert_viking_csv_to_qif(args.usage_report, args.output, account, category)
