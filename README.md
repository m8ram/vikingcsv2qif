# vikingcsv2qif

Convert usage reports from Mobile Vikings from CSV to QIF format.
The resulting QIF file can then be imported in e.g. GnuCash.

## QIF format

The description of the imported transactions contains:
- The type of the transaction as mentioned in the usage report
- the human readable duration for voice calls / the amount of data transferred for data transfers
- the number dialed/received for voice calls and text messages

### Sample output

If an account is configured the file starts with an account record:

```
!Account
NAssets:Current Assets:Calling Credit
^
```

The type is always set to "Property & Debt:Asset".

```
!Type:Oth A
```

Followed by the individual transactions. The category (code 'L') is added only
if a category has been set.

```
D24/06/2018
T-0.10
MText message sent  123456789
LExpenses:Utilities:Mobile phone
^
```

```
D24/06/2018
T-0.20
MVoice call made 0:28 123456789
LExpenses:Utilities:Mobile phone
^
```

```
D24/06/2017
T-0.10
MData 0.51 MB
LExpenses:Utilities:Mobile phone
^
```

## Prerequisites

You need python3 to run vikingcsv2qif.

## Running vikingcsv2qif

- Download the usage report from the Mobile Viking website.
- Optionally create a configuration file (default is ~/.vikingcsv2qif)
- Pass the path to the downloaded file and the desired output file to vikingcsv2qif
- Every transaction with a positive amount in the Price column will be converted.
- If no account is specified the GnuCash import wizard will allow you to select one.
- If no category is selected the GnuCash import wizard will present a list of all converted transactions and allow you to select a matching account for each.  
Especially for larger files specifying the category is recommended!

### Usage

```
usage: vikingcsv2qif.py [-h] [-a ACCOUNT] [-c CATEGORY] [--config CONFIG]
                        usage_report output

Convert Mobile viking CSV usage report to QIF format

positional arguments:
  usage_report
  output

optional arguments:
  -h, --help            show this help message and exit
  -a ACCOUNT, --account ACCOUNT
                        QIF account name (Asset account in GnuCash)
  -c CATEGORY, --category CATEGORY
                        Category for the transactions (Expense account in
                        GnuCash)
  --config CONFIG       Optional configuration file
```

### Configuration file

At startup vikingcsv2qif will read a configuration file if the `--account` or `--category` arguments are not passed. This allows you to save this information for future use.

The file is in standard INI format:

```
[vikingcsv2qif]
account = Assets:Current Assets:Calling Credit
category = Expenses:Utilities:Mobile phone
```

vikingcsv2qif will read the configuration file '.vikingcsv2qif.conf' in your home dir by default but you can pass the name and path of different file with the `--config` option.

## Authors

* **Bram Mertens** - *Initial work*

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* [Instruction on how to convert CSV to QIF in the Gnucash FAQ](https://wiki.gnucash.org/wiki/FAQ#Q:_How_do_I_convert_from_CSV.2C_TSV.2C_XLS_.28Excel.29.2C_or_SXC_.28OpenOffice.org_Calc.29_to_a_QIF.3F)
* Initial inspiration came from Nils Andresen's script sent to the [Gnucash-de mailing list](https://lists.gnucash.org/pipermail/gnucash-de/2010-March/007310.html).
* [Description of the QIF format](https://en.wikipedia.org/wiki/Quicken_Interchange_Format)
